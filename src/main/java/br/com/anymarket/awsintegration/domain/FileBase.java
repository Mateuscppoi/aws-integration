package br.com.anymarket.awsintegration.domain;

public final class FileBase {

    public static final String BASE = "Windows Registry Editor Version 5.00\n" +
            "\n" +
            "[HKEY_CURRENT_USER\\Software\\SimonTatham]\n" +
            "\n" +
            "[HKEY_CURRENT_USER\\Software\\SimonTatham\\PuTTY]\n" +
            "\n" +
            "[HKEY_CURRENT_USER\\Software\\SimonTatham\\PuTTY\\Sessions]";

    public static final String REPLACED_LINUX = "Host {server-name}\n" +
            "\tHostName {server-ip}\n" +
            "\tUser ec2-user\n" +
            "\tIdentitiesOnly yes\n" +
            "\tIdentityFile {ssh-key-path}";

    public static final String REPLACED_WINDOWS = "[HKEY_CURRENT_USER\\Software\\SimonTatham\\PuTTY\\Sessions\\{server-name}]\n" +
            "\"HostName\"=\"ec2-user@{server-ip}\"\n" +
            "\"PublicKeyFile\"={ssh-key-path}";
}
