package br.com.anymarket.awsintegration.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ServerInfo {

    private String serverName;
    private String ip;
}
