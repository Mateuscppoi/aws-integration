package br.com.anymarket.awsintegration.service;

import br.com.anymarket.awsintegration.domain.FileBase;
import br.com.anymarket.awsintegration.domain.ServerInfo;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Tag;
import com.google.gson.Gson;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

@Log4j2
@Service
public class EC2Service {

    @Value("${project.name}")
    private String projectName;

    @Value("${project.linux.ssh-key-path}")
    private String linuxKeyPath;

    @Value("${project.windows.ssh-key-path}")
    private String windowsKeyPath;

    private final Path rootLocation = Paths.get("files-dir");

    private final AmazonEC2 usClient;

    private final AmazonEC2 saClient;

    private final AtomicBoolean isRecreating = new AtomicBoolean(false);

    public EC2Service(@Qualifier("saEastClient") AmazonEC2 usClient, @Qualifier("usEastClient") AmazonEC2 saClient) {
        this.usClient = usClient;
        this.saClient = saClient;
    }

    @SneakyThrows
    @PostConstruct
    public void deleteAndCreateDir() {
        FileSystemUtils.deleteRecursively(rootLocation.toFile());
        Files.createDirectories(rootLocation);
        recreateFiles();
    }

    @SneakyThrows
    public void recreateFiles() {
        isRecreating.set(true);
        generateWindowsFile(true);
        generateLinuxFile(true);
        isRecreating.set(false);
    }

    @SneakyThrows
    public FileSystemResource generateWindowsFile(Boolean isSchedule) {
        verifySchedule(isSchedule);

        File file = getFile(rootLocation, projectName, "-aws-machines.reg");

        if (!file.exists() || isSchedule) {
            try (FileWriter writer = new FileWriter(file)) {
                writer.write(FileBase.BASE);
                rewriteFile(writer, FileBase.REPLACED_WINDOWS, windowsKeyPath);
            }
        }
        return new FileSystemResource(file);
    }

    @SneakyThrows
    public FileSystemResource generateLinuxFile(Boolean isSchedule) {
        verifySchedule(isSchedule);

        File file = getFile(rootLocation, projectName, "-aws-machines");

        if (!file.exists() || isSchedule) {
            try (FileWriter writer = new FileWriter(file)) {
                rewriteFile(writer, FileBase.REPLACED_LINUX, linuxKeyPath);
            }
        }
        return new FileSystemResource(file);
    }

    private void verifySchedule(Boolean isSchedule) throws InterruptedException {
        if (isRecreating.get() && !isSchedule) {
            log.info("Recriando arquivos, aguardando por 2 segundos");
            Thread.sleep(2000);
        }
    }

    private File getFile(Path rootLocation, String appName, String suffix) {
        String pathToFile = rootLocation.toString() + "/" + appName + suffix;
        return new File(pathToFile);
    }

    @SneakyThrows
    private void rewriteFile(FileWriter writer, String replaced, String keyPath) {
        writer.write("\n\n");

        List<ServerInfo> serverInfos = getServerInfos();

        serverInfos.sort(Comparator.comparing(ServerInfo::getServerName));

        serverInfos.forEach(instance -> rewriteFile(writer, instance, replaced, keyPath));
    }

    private void rewriteFile(FileWriter writer, ServerInfo instance, String fileBase, String keyPath) {
        String finalReplace = fileBase.replaceAll("\\{server-name\\}", instance.getServerName());
        finalReplace = finalReplace.replaceAll("\\{server-ip\\}", instance.getIp());
        finalReplace = finalReplace.replaceAll("\\{ssh-key-path\\}", keyPath);
        try {
            writer.write(finalReplace);
            writer.write("\n\n");
        } catch (IOException e) {
            log.error("Ocorreu um erro ao reescrever o arquivo do server {}", new Gson().toJson(instance));
        }
    }

    private List<ServerInfo> getServerInfos() {
        DescribeInstancesRequest request = new DescribeInstancesRequest();
        DescribeInstancesResult responseSa = saClient.describeInstances(request);
        DescribeInstancesResult responseUs = usClient.describeInstances(request);

        List<ServerInfo> serverInfos = responseSa.getReservations().get(0).getInstances().stream().map(this::getServerInfo).collect(Collectors.toList());
        serverInfos.addAll(responseUs.getReservations().get(0).getInstances().stream().map(this::getServerInfo).collect(Collectors.toList()));
        return serverInfos;
    }

    private ServerInfo getServerInfo(Instance instance) {
        String serverName = instance.getTags().stream().filter(t -> t.getKey().equals("Name")).findAny().map(Tag::getValue).orElseThrow(() -> new RuntimeException("Deu xabu"));
        String ip = instance.getPublicIpAddress();
        return new ServerInfo(serverName, ip);
    }
}
