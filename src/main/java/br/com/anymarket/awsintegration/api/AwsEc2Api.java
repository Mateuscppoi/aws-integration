package br.com.anymarket.awsintegration.api;

import br.com.anymarket.awsintegration.service.EC2Service;
import lombok.extern.log4j.Log4j2;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

@Log4j2
@RestController
@RequestMapping("/ec2")
public class AwsEc2Api {

    private final EC2Service ec2Service;

    public AwsEc2Api(EC2Service ec2Service) {
        this.ec2Service = ec2Service;
    }

    @GetMapping("/windows")
    public ResponseEntity<?> windows() {
        FileSystemResource generatedWindowsFile = ec2Service.generateWindowsFile(false);

        return getBody(generatedWindowsFile);
    }

    @GetMapping("/linux")
    public ResponseEntity<?> linux() {
        FileSystemResource generatedLinuxFile = ec2Service.generateLinuxFile(false);

        return getBody(generatedLinuxFile);
    }

    @PostMapping("/recreateFiles")
    @Scheduled(cron = "0 0 0/3 ? * *")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void recreateExistingFiles() {
        log.info("Recriando diretorio");
        ec2Service.recreateFiles();
        log.info("Finalizado recriação do diretorio");
    }

    private ResponseEntity<FileSystemResource> getBody(FileSystemResource generatedFile) {
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + generatedFile.getFile().getName() + "\"")
                .body(generatedFile);
    }

}
