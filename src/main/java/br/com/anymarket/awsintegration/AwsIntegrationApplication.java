package br.com.anymarket.awsintegration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class AwsIntegrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(AwsIntegrationApplication.class, args);
	}

}
