# ***AWS INTEGRATION***
Projeto para integrar com a SDK da AWS para permitir uma implementação customizada com base na necessidade da equipe.

## ***Geração de arquivos para conexão SSH***
Esse projeto permite a criação de um arquivo .reg (Windows) e também para Linux que contém todas as máquinas que estejam 
localizadas em São Paulo (sa-east-1) ou no Norte da Virgínia (us-east-1).

Uma rotina a cada 3 horas mantém os arquivos atualizados.

## ***Variáveis de ambiente do Projeto***

| Variável | Descrição | Tipo | Valor padrão |
|:---|:---|:---|:---|
| AWS_ACCESS_KEY | Access Key do usuário da AWS. | string |
| AWS_SECRET_KEY | Secret Key do usuário da AWS. | string |
| LINUX_SSH_KEY_LOCATION | Localização da chave de acesso do servidor no linux. | string |
| WINDOWS_SSH_KEY_LOCATION | Localização da chave de acesso do servidor no windows. | string |
| PROJECT_NAME | Nome do projeto (utilizado para nomear os arquivos gerados). | string | project


### ***Endpoints***
- /ec2/windows
    - Método: Get
    - Gera um arquivo com a extensão .reg contendo todas as máquinas que estejam localizadas em São Paulo (sa-east-1) ou no Norte da Virgínia (us-east-1).
    - O arquivo já é baixado automaticamente.

- /ec2/linux
    - Método: Get
    - Gera um arquivo sem extensão contendo todas as máquinas que estejam localizadas em São Paulo (sa-east-1) ou no Norte da Virgínia (us-east-1).
    - O arquivo já é baixado automaticamente.
    
- /ec2/recreateFiles
    - Método: Post
    - Recria o diretório e realiza uma nova criação dos arquivos.
    - Uma rotina é executada a cada 3 horas para atualização dos arquivos. 
